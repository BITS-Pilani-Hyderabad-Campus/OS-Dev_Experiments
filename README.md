# OS_Experiments

Code dump for my experiments with operating systems. I hope it's well-commented, it's primarily an attempt to get people from college interested in systems programming.

I'll try to only push code that builds and runs as intended. So updates from my side might be a bit rare, as 3rd year is turning out to be a bit heavy :P .

## How to build

You'll need -
- A Linux machine
- GCC cross-compiler targeting the required architecture
  - x86_64-none-elf for the x86_64 architecture
  - (That's the only architecture available for now)
  - Refer to [The OSDev Wiki][847fd1e4] and adapt the steps there to build the cross compiler
- NASM
- Grub `sudo apt-get install grub` on a Debian-based system or `sudo pacman -S grub` on Arch, or whatever your distro requires.
- Xorisso (might be available as part of a different package, on your distribution) `sudo pacman -S libisoburn` on Arch
- QEMU / Bochs / VirtualBox / Your machine (if you're willing to risk it.)
  [847fd1e4]: http://wiki.osdev.org/GCC_Cross-Compiler "How to build a GCC Cross Compiler"

Build scripts are available in the **core** directory. Try **qemu.sh** to build the iso and run on QEMU. Or **iso.sh** to just build the ISO. **build.sh** builds the kernel ELF executable. **clean.sh** cleans all the build directories and files.

## Coding styleguide

### Functions

Public functions should follow the following layout
```
return type
fileName_functionNameInCamelCase( type1 param1, type2 param2 );
```
Internal implementation functions should have the `static` keyword and should be follow the following layout
```
static return type
_functionNameInCamelCase( type1 param1, type2 param2 );
```

### Structures

Structures should not be `typedef`d, and their names should follow the layout `struct StructName`

### Constants

Constants should either be declared with the `const` keyword, or should be declared in the form of `#define CONSTANT value`. Their names should have all the letters capitalized, and words separated with underscores. For instance, `CONSTANT_NAME`.

### Variables

Global variables should follow the layout `GlobalVariable`. Local variables should follow the format `local_variable`. Static variables should follow the format `_static_variable`.
