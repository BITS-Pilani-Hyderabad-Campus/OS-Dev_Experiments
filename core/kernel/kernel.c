// #####################################################################
// This is where the real kernel code is. Or, rather, the central place
// which calls a bunch of other functions
// #####################################################################

#include <kernel/tty.h>
#include <kernel/multiboot.h>
#include <kernel/global_vars.h>
#include <kernel/hal/hal.h>
#include <kernel/memory/p_mem_mngr.h>

#include <stdio.h>
#include <stdlib.h>

// -------------------------------------------------------------
// External variables defined in the linker script
// -------------------------------------------------------------

extern int KernelTextStart;
extern int KernelDataEnd;
extern int KernelBssEnd;

// #####################################################################
// Set up the environment for the kernel
// PARAM :-
//      void* pointer_to_multiboot_mem_map
// RET   :-
// #####################################################################

void
kernel_early( void*  multiboot_info_table ) {
    // -------------------------------------------------------------
    // Clear the screen an initialize the terminal
    // -------------------------------------------------------------

    terminal_initialize();

    // -------------------------------------------------------------
    // Initialize the hardware abstraction layer
    // -------------------------------------------------------------

    hal_initialize();

    // -------------------------------------------------------------
    // Start filling the global variables
    // -------------------------------------------------------------

    struct BootInfoFixed* boot_info = multiboot_info_table;
    MultibootInfoTableAddr = boot_info;
    MultibootInfoTableEndAddr = ( multiboot_info_table + boot_info->total_size );

    // -------------------------------------------------------------
    // Initialize the physical memory manager
    // -------------------------------------------------------------

    /* Calculate the size of memory, to be passed to the physical memory manager */
    struct MemoryMapTag* mem_map_tag = multiboot_getMemMapTag( boot_info );
    if( !mem_map_tag ) {
        printf( "Error! Multiboot memory map tag not found. Aborting...\n" );
        abort( );
    }
    size_t max = 0;
    struct MemoryArea* mem_area = multiboot_getFirstMemArea( mem_map_tag );
    if( !mem_area ) {
        printf( "Error! First multiboot memory area not found. Aborting...\n" );
        abort( );
    }
    while( mem_area ) {
        if( mem_area->base + mem_area->length > max )
            max = mem_area->base + mem_area->length;
        mem_area = multiboot_getNextMemArea( mem_map_tag, mem_area );
    }
    /* max now has the size of physical memory */

    /* Now calculate the end address of either the kernel or the multiboot indo
    table (whichever comes later), so as to put the bitmap of the physical memory
    manager there */
    void* addr = ( void* ) MultibootInfoTableEndAddr;
    addr++;

    /* Now call the init function of the physical memory manager */
    pMemMngr_init( max, addr );

    /* Now init regions marked as available in the multiboot memory map */
    mem_area = multiboot_getFirstMemArea( mem_map_tag );
    while( mem_area ) {
        if( mem_area->type == 1 )
            pMemMngr_initRegion( ( void* )mem_area->base, mem_area->length );
        else if( mem_area->type > 4 || mem_area->type < 1 )
            mem_area->type = 2;     /* Marked undefined regions as reserved */
        mem_area = multiboot_getNextMemArea( mem_map_tag, mem_area );
    }

    /* Now de-init the region occupied by the kernel, the multiboot header table, and the
    physical memory manager bitmap */
    pMemMngr_deinitRegion( ( void* ) &KernelTextStart, ( ( size_t ) &KernelBssEnd - ( size_t ) &KernelTextStart ) );
    pMemMngr_deinitRegion( ( void* ) MultibootInfoTableAddr, ( ( size_t ) MultibootInfoTableEndAddr - ( size_t ) MultibootInfoTableAddr ) );
    pMemMngr_deinitRegion( addr, pMemMngr_getBlockCount( ) / 64 );
}

// #####################################################################
// The main kernel
// PARAM :-
// RET   :-
// #####################################################################

void
kernel_main( ) {
    printf( "Tick count: %u\n", hal_getTickCount() );

    printf( "Text_start: 0x%x, BSS_end: 0x%x\n", &KernelTextStart, &KernelBssEnd );

    printf( "Multiboot_header:\nStart: 0x%x, End: 0x%x\n", MultibootInfoTableAddr, MultibootInfoTableEndAddr );

    printf( "Tick count: %u\n", hal_getTickCount() );
}
