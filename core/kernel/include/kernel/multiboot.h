// #####################################################################
// Interface for retrieving information from the multiboot information
// table passed to the kernel by the bootloader
// #####################################################################

#ifndef _KERNEL_MULTIBOOT_H_
#define _KERNEL_MULTIBOOT_H_ 1

#include <stdint.h>

// #####################################################################
// Useful structures
// #####################################################################

/* The fixed part of the multiboot info table */
struct BootInfoFixed {
    uint32_t    total_size;
    uint32_t    reserved;
    // Folowed by a series of tags
} __attribute__((packed));

/* General tag structure */
struct Tag {
    uint32_t    type;
    uint32_t    size;
    // Followed by tag-specific fields
} __attribute__((packed));

// -------------------------------------------------------------
// ELF sections
// -------------------------------------------------------------

/* ELF sections tag header */
struct ElfSectionsTag {
    uint32_t    type;
    uint32_t    size;
    uint32_t    num_sections;
    uint32_t    entry_size;
    uint32_t    shndx;   // String table
    // Followed by a list of sections
} __attribute__((packed));

/* An entry in the ELF sections table */
struct ElfSection {
    uint32_t    name;
    uint32_t    type;
    uint64_t    flags;
    uint64_t    addr;
    uint64_t    offset;
    uint64_t    size;
    uint32_t    link;
    uint32_t    info;
    uint64_t    addr_align;
    uint64_t    entry_size;
} __attribute__((packed));

// -------------------------------------------------------------
// Memory map
// -------------------------------------------------------------

/* The memory map tag header */
struct MemoryMapTag {
    uint32_t    type;
    uint32_t    size;
    uint32_t    entry_size;
    uint32_t    entry_version;
} __attribute__((packed));

/* An entry in the multiboot memory map tag */
struct MemoryArea {
    uint64_t    base;
    uint64_t    length;
    uint32_t    type;
    uint32_t    reserved;
} __attribute__((packed));

// #####################################################################
// Interface functions and macros
// #####################################################################

// -------------------------------------------------------------
// Fixed part tag
// -------------------------------------------------------------

/* Return the fixed part of the multiboot info table */
inline struct BootInfoFixed*
multiboot_load( const void* multiboot_info_table_addr ) {
    return ( struct BootInfoFixed* ) multiboot_info_table_addr;
}

/* Get end address of table */
inline void*
multiboot_getEndAddr( const struct BootInfoFixed* table ) {
    void* void_addr = ( void* ) table;
    return void_addr + table->total_size;
}

/* Get the address of the first tag */
inline struct Tag*
multiboot_getFirstTag( const struct BootInfoFixed* table ) {
    void* addr = ( void* ) table;
    addr += 8;
    return ( struct Tag* ) addr;
}

// -------------------------------------------------------------
// Generic tags
// -------------------------------------------------------------

/* Get address of tag given type. NULL if not present */
struct Tag*
multiboot_getTagType ( const struct BootInfoFixed* table, uint32_t type );

/* Get address of the next tag. NULL if not present */
struct Tag*
multiboot_getNextTag( const struct Tag* tag );

// -------------------------------------------------------------
// Memory map
// -------------------------------------------------------------

/* Get address of memory map tag. NULL if not present */
inline struct MemoryMapTag*
multiboot_getMemMapTag( const struct BootInfoFixed* table ) {
    return ( struct MemoryMapTag* ) multiboot_getTagType( table, 6 );
}

/* Get address of first memory area entry */
struct MemoryArea*
multiboot_getFirstMemArea( const struct MemoryMapTag* memory_map_tag );

/* Get address of next memory area. Return NULL if not available */
struct MemoryArea*
multiboot_getNextMemArea( const struct MemoryMapTag* memory_map_tag, const struct MemoryArea* memory_area );

// -------------------------------------------------------------
// ELF sections
// -------------------------------------------------------------

/* Get address of ELF sections tag. NULL if not present */
inline struct ElfSectionsTag*
multiboot_getElfSectionsTag( const struct BootInfoFixed* table ) {
    return ( struct ElfSectionsTag* ) multiboot_getTagType( table, 9 );
}

/* Get address of ELF section given section's index in table */
struct ElfSection*
multiboot_getElfSectionNum( const struct ElfSectionsTag* elf_section_tag, uint32_t elf_section_num );

#endif
