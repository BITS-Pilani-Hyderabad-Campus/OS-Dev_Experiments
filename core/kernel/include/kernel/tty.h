// #####################################################################
// Interface for interacting with a terminal. This is intended to be
// independent of the graphics mode
// #####################################################################

#ifndef _KERNEL_TTY_H_
#define _KERNEL_TTY_H_ 1

#include <kernel/vga.h>

#include <stddef.h>

// #####################################################################
// Interface functions
// #####################################################################

/* Clear the screen at place the cursor at the bottom left of the screen */
void
terminal_initialize( );

/* Set the colour attributes of the terminal */
void
terminal_setColour( enum VgaColour foreground, enum VgaColour background );

/* Write a string of a given length to the terminal */
void
terminal_write( const char* str, size_t len );

/* Write a null-terminated string out to the terminal and return its length
(number of characters printed) */
size_t
terminal_writeString( const char* str );

#endif
