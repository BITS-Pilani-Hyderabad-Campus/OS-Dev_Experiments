// #####################################################################
// Interface without an implementation. Contains macros and data members
// regarding the VGA text buffer
// #####################################################################

#ifndef _KERNEL_VGA_H_
#define _KERNEL_VGA_H_ 1

#include <stdint.h>

/* For representing colour codes */
enum VgaColour {
    COLOUR_BLACK         = 0,
    COLOUR_BLUE          = 1,
    COLOUR_GREEN         = 2,
    COLOUR_CYAN          = 3,
    COLOUR_RED           = 4,
    COLOUR_MAGENTA       = 5,
    COLOUR_BROWN         = 6,
    COLOUR_LIGHT_GREY    = 7,
    COLOUR_DARK_GREY     = 8,
    COLOUR_LIGHT_BLUE    = 9,
    COLOUR_LIGHT_GREEN   = 10,
    COLOUR_LIGHT_CYAN    = 11,
    COLOUR_LIGHT_RED     = 12,
    COLOUR_LIGHT_MAGENTA = 13,
    COLOUR_LIGHT_BROWN   = 14,
    COLOUR_WHITE         = 15
};

/* Constants */
#define VGA_WIDTH   80
#define VGA_HEIGHT  25
#define VGA_BUFFER  0xB8000

/* Macros  */
inline uint8_t
vga_makeColour( enum VgaColour fg, enum VgaColour bg ) {
    return ( fg | ( bg << 4 ) );
}

inline uint16_t
vga_makeEntry ( char c, uint8_t colour ) {
    return ( ( uint16_t ) c | ( ( ( uint16_t ) colour ) << 8 ) );
}

// #####################################################################
// Useful definitions
// #####################################################################

/* Ports */
#define VGA_COMMAND_REG 0x03D4
#define VGA_DATA_REG    0x03D5
/* Command bytes */
#define VGA_HIGH_BYTE_COMMAND   14
#define VGA_LOW_BYTE_COMMAND    15

#endif
