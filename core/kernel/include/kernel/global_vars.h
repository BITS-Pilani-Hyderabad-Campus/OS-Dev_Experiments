// #####################################################################
// Global variables which can be accessed by various parts of the kernel
// #####################################################################

#ifndef _KERNEL_GLOBAL_VARS_H_
#define _KERNEL_GLOBAL_VARS_H_ 1

#include <kernel/multiboot.h>

// #####################################################################
// Multiboot info table
// #####################################################################

extern struct BootInfoFixed* MultibootInfoTableAddr;
extern struct BootInfoFixed* MultibootInfoTableEndAddr;

#endif
