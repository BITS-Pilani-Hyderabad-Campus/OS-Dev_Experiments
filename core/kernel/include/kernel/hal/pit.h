// #####################################################################
// Interface for the Programmable Interval Timer
// #####################################################################

#ifndef _KERNEL_HAL_PIT_H_
#define _KERNEL_HAL_PIT_H_ 1

#include <kernel/hal/hal.h>

#include <stdbool.h>
#include <stddef.h>

// #####################################################################
// Useful definitions
// #####################################################################

//-----------------------------------------------
//	Operational Command Bit masks
//-----------------------------------------------

#define PIT_OCW_MASK_BINCOUNT   0x01    //00000001
#define PIT_OCW_MASK_MODE       0x0E    //00001110
#define PIT_OCW_MASK_RL         0x30    //00110000
#define PIT_OCW_MASK_COUNTER    0xC0    //11000000

//-----------------------------------------------
//	Operational Command control bits
//-----------------------------------------------

/* Use when setting binary count mode */
#define PIT_OCW_BINCOUNT_BINARY 0   //00000000
#define PIT_OCW_BINCOUNT_BCD    1   //00000001

/* Use when setting counter mode */
#define PIT_OCW_MODE_TERMINALCOUNT  0x0 //0000
#define PIT_OCW_MODE_ONESHOT        0x2 //0010
#define PIT_OCW_MODE_RATEGEN        0x4 //0100
#define PIT_OCW_MODE_SQUAREWAVEGEN  0x6 //0110
#define PIT_OCW_MODE_SOFTWARETRIG   0x8 //1000
#define PIT_OCW_MODE_HARDWARETRIG   0xA //1010

/* Use when setting data transfer */
#define PIT_OCW_RL_LATCH    0x00    //000000
#define PIT_OCW_RL_LSBONLY  0x10    //010000
#define PIT_OCW_RL_MSBONLY  0x20    //100000
#define PIT_OCW_RL_DATA     0x30    //110000

/* Use when setting the counter we are working with */
#define PIT_OCW_COUNTER_0   0x00    //00000000
#define PIT_OCW_COUNTER_1   0x40    //01000000
#define PIT_OCW_COUNTER_2   0x80    //10000000

//-----------------------------------------------
//	Controller Registers
//-----------------------------------------------

#define PIT_REG_COUNTER0    0x40
#define PIT_REG_COUNTER1    0x41
#define PIT_REG_COUNTER2    0x42
#define PIT_REG_COMMAND     0x43

// #####################################################################
// Interface functions
// #####################################################################

/* Send a command byte to the PIT */
inline void
pit_sendCommand( uint8_t command ) {
    hal_outb( PIT_REG_COMMAND, command );
}

/* write data byte to a counter */
inline void
pit_sendData( uint16_t data, uint8_t counter ) {
    uint16_t port = ( counter == PIT_OCW_COUNTER_0 ) ? PIT_REG_COUNTER0 : ( ( counter == PIT_OCW_COUNTER_1 ) ? PIT_REG_COUNTER1 : PIT_REG_COUNTER2 );
	hal_outb( port, ( uint8_t ) data );
}

/* Reads data from a counter */
inline
uint8_t pit_readData( uint16_t counter ) {
    uint16_t port = ( counter == PIT_OCW_COUNTER_0 ) ? PIT_REG_COUNTER0 : ( ( counter == PIT_OCW_COUNTER_1 ) ? PIT_REG_COUNTER1 : PIT_REG_COUNTER2 );
    return hal_inb( port );
}

/* Sets new pit tick count and returns prev. value */
size_t
pit_setTickCount( size_t i );

/* Returns current tick count */
size_t
pit_getTickCount( );

/* Starts a counter. Counter continues until another call to this routine */
void
pit_startCounter( uint32_t freq, uint8_t counter, uint8_t mode );

/* Initialize the PIT */
void
pit_initialize( );

#endif
