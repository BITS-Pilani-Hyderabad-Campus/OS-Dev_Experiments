// #####################################################################
// Interface for interacting with the PIC
// #####################################################################

#ifndef _KERNEL_HAL_PIC_H_
#define _KERNEL_HAL_PIC_H_ 1

#include <kernel/hal/hal.h>

#include <stdint.h>

// #####################################################################
// Useful definitions
// #####################################################################

//-----------------------------------------------
//	Devices connected to the PICs. May be useful
//	when enabling and disabling IRQs
//-----------------------------------------------

/* The following devices use PIC 1 to generate interrupts */
#define PIC_IRQ_TIMER       0
#define PIC_IRQ_KEYBOARD    1
#define PIC_IRQ_SERIAL2     3
#define PIC_IRQ_SERIAL1     4
#define PIC_IRQ_PARALLEL2   5
#define PIC_IRQ_DISKETTE    6
#define PIC_IRQ_PARALLEL1   7

/* The following devices use PIC 2 to generate interrupts */
#define PIC_IRQ_CMOSTIMER   0
#define PIC_IRQ_CGARETRACE  1
#define PIC_IRQ_AUXILIARY   4
#define PIC_IRQ_FPU         5
#define PIC_IRQ_HDC         6

//-----------------------------------------------
//	Command words are used to control the devices
//-----------------------------------------------

/* Command Word 2 bit masks. Use when sending commands */
#define PIC_OCW2_MASK_L1        0x01    //00000001
#define PIC_OCW2_MASK_L2        0x02    //00000010
#define PIC_OCW2_MASK_L3        0x04    //00000100
#define PIC_OCW2_MASK_EOI       0x20    //00100000
#define PIC_OCW2_MASK_SL        0x40    //01000000
#define PIC_OCW2_MASK_ROTATE    0x80    //10000000

/* Command Word 3 bit masks. Use when sending commands */
#define PIC_OCW3_MASK_RIS   0x01    //00000001
#define PIC_OCW3_MASK_RIR   0x02    //00000010
#define PIC_OCW3_MASK_MODE  0x04    //00000100
#define PIC_OCW3_MASK_SMM   0x20    //00100000
#define PIC_OCW3_MASK_ESMM  0x40    //01000000
#define PIC_OCW3_MASK_D7    0x80    //10000000

//-----------------------------------------------
//	Controller Registers
//-----------------------------------------------

/* PIC 1 register port addresses */
#define PIC1_REG_COMMAND    0x20
#define PIC1_REG_STATUS     0x20
#define PIC1_REG_DATA       0x21
#define PIC1_REG_IMR        0x21

/* PIC 2 register port addresses */
#define PIC2_REG_COMMAND    0xA0
#define PIC2_REG_STATUS     0xA0
#define PIC2_REG_DATA       0xA1
#define PIC2_REG_IMR        0xA1

// #####################################################################
// Interface functions
// #####################################################################

/* Read a data byte from a PIC */
uint8_t
pic_readData( uint8_t pic_num );

/* Send a data byte to a PIC */
void
pic_sendData( uint8_t pic_num, uint8_t data );

/* Send a command to a PIC */
void
pic_sendCommand( uint8_t pic_num, uint8_t command );

/* Initialize the PICs */
void
pic_initialize( uint8_t base_0, uint8_t base_1 );

#endif
