// #####################################################################
// Interface for the hardware abstraction layer.
// This acts as an interface for platform-dependent hardware interaction
// #####################################################################

#ifndef _KERNEL_HAL_HAL_H_
#define _KERNEL_HAL_HAL_H_ 1

#include <stdint.h>

// #####################################################################
// Interface functions
// #####################################################################

/* Initialize the hardware abstraction layer */
void
hal_initialize();

/* Shut down the hardware abstraction layer */
void
hal_shutdown();

/* Write a byte out to a device using memory mapped IO */
void
hal_outb( uint16_t port_num, uint8_t  data );

/* Read a byte from a device using memory mapped IO */
uint8_t
hal_inb( uint16_t port_num );

/* Read a word from a device using memory mapped IO */
uint16_t
hal_inw( uint16_t port_num );

/* Disable interrupts */
void
hal_disableInterrupts();

/* Enable interrupts */
void
hal_enableInterrupts();

/* Notify the HAL that a device interrupt is done */
void
hal_interruptDone( uint8_t int_num );

/* Get CPU vendor string */
char*
hal_getCpuVendor();

/* Get the tick count from the PIT */
uint32_t
hal_getTickCount();

#endif
