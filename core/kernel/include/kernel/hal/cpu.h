// #####################################################################
// Interface for accessing the CPU structures
// #####################################################################

#ifndef _KERNEL_HAL_CPU_H_
#define _KERNEL_HAL_CPU_H_ 1

// #####################################################################
// Interface functions
// #####################################################################

/* Initialize the CPU structures */
void
cpu_initialize( );

/* Shut down the CPU */
void
cpu_shutdown( );

/* Get the CPU vendor string */
char*
cpu_getVendor( );

#endif
