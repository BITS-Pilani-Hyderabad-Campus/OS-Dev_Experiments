// #####################################################################
// Interface for IDT and interrupts
// #####################################################################

#ifndef _KERNEL_HAL_IDT_H_
#define _KERNEL_HAL_IDT_H_ 1

#include <stdint.h>

// #####################################################################
// Interface functions
// #####################################################################

/* Initialize the IDT with default interrupt handlers */
void
idt_initialize( );

/* Register an interrupt handler to the given vector */
void
idt_registerHandler( uint8_t int_num, void* base_addr );

#endif
