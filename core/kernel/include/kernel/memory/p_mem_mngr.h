// #####################################################################
// This is the part of the memory module that we'll use to allocate
// frames of physical memory when required.
// This is now very basic, and I plan to improve the algorithms greatly
// in order to promote scalability. Or even basic sense.
// That's why the goal is to make this easily plug-in-able, and the
// interface neutral of the implementation, so that I can upgrade the
// implementation later without breaking anything.
// #####################################################################

#ifndef _KERNEL_MEMORY_PMEMMNGR_H_
#define _KERNEL_MEMORY_PMEMMNGR_H_ 1

#include <stddef.h>

// #####################################################################
// Useful definitions
// #####################################################################

#define PAGE_SIZE   0x1000  // Size of a page in bytes

// #####################################################################
// Interface functions
// #####################################################################

/* Initialize the physcal memory manager. Sets up the bitmap of available and
free frames */
void
pMemMngr_init( size_t memory_size, void* bitmap_addr );

/* Enables a stretch of physical memory for use */
void
pMemMngr_initRegion( void* base_addr, size_t len );

/* Disables a stretch of physical memory for use */
void
pMemMngr_deinitRegion( void* base_addr, size_t len );

/* Allocates a single memory block and returns its base address */
void*
pMemMngr_alloc();

/* Allocates n contiguous blocks of memory */
void*
pMemMngr_allocN( size_t n );

/* Deallocates (frees) a single memory block given base address */
void
pMemMngr_free( void* base_addr );

/* Deallocates n contiguous blocks given the base address and n */
void
pMemMngr_freeN( void* base_addr, size_t n );

/* Get the total size of physical memory in bytes */
size_t
pMemMngr_getMemorySize( );

/* Get the total number of memory blocks */
size_t
pMemMngr_getBlockCount( );

/* Get the number of used blocks */
size_t
pMemMngr_getUsedBlockCount( );

/* Get the number of free blocks */
size_t
pMemMngr_getFreeBlockCount( );

/* Get the size of one blocl */
size_t
pMemMngr_getBlockSize( );

#endif
