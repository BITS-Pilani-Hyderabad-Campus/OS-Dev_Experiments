// #####################################################################
// Storage of the global variables externed in kernel/global_vars.h
// #####################################################################

#include <kernel/global_vars.h>

// #####################################################################
// Multiboot info table
// #####################################################################

struct BootInfoFixed* MultibootInfoTableAddr;
struct BootInfoFixed* MultibootInfoTableEndAddr;
