// #####################################################################
// Implementation of the functions defined in the kernel/tty.h interface
// Functions for interacting with the terminal
// #####################################################################

#include <kernel/tty.h>
#include <kernel/hal/hal.h>

#include <string.h>

// #####################################################################
// Internal data members
// #####################################################################

static size_t _column;
static uint16_t* _buffer;
static uint8_t _colour;

// #####################################################################
// Internal functions
// #####################################################################

/* Update the hardware cursor */
static void
_updateCursor( ) {
    size_t index = ( ( VGA_HEIGHT - 1 ) * VGA_WIDTH ) + _column;
    hal_outb( VGA_COMMAND_REG, VGA_HIGH_BYTE_COMMAND );
    hal_outb( VGA_DATA_REG, ( uint8_t ) ( ( index >> 8 ) & 0xFF ) );
    hal_outb( VGA_COMMAND_REG, VGA_LOW_BYTE_COMMAND );
    hal_outb( VGA_DATA_REG, ( uint8_t ) ( index & 0xFF ) );
}

/* Newline character. Scroll and clear the bottom line of the buffer */
static void
_newLine( ) {
    size_t i;
    for( i = 0; i < ( VGA_HEIGHT - 1 ) * VGA_WIDTH; i++ ) {
        _buffer[i] = _buffer[i + VGA_WIDTH];
    }
    uint16_t entry = vga_makeEntry( ' ', _colour );
    for( i = ( ( VGA_HEIGHT - 1 ) * VGA_WIDTH ); i < ( VGA_HEIGHT * VGA_WIDTH ); i++ ) {
        _buffer[i] = entry;
    }
    _column = 0;
    _updateCursor( );
}

/* Put a character on the current location on the screen */
static void
_putchar( char c ) {
    if( c == '\n' )
        return _newLine( );
    uint16_t entry = vga_makeEntry( c, _colour );
    size_t index = ( ( VGA_HEIGHT - 1 ) * VGA_WIDTH ) + _column;
    _buffer[index] = entry;
    if( ++_column >= VGA_WIDTH ) {
        _newLine( );
    }
}

// #####################################################################
// Interface fuction implementation
// #####################################################################

/* Clear the screen and place the terminal cursor on the bottom left corner */

void
terminal_initialize( ) {
    _column = 0;
    _colour = vga_makeColour( COLOUR_LIGHT_GREEN, COLOUR_BLACK );
    _buffer = ( uint16_t* ) VGA_BUFFER;
    uint16_t entry = vga_makeEntry( ' ', _colour );
    for( size_t index = 0; index < ( VGA_HEIGHT * VGA_WIDTH ); index++ )
        _buffer[index] = entry;
    _updateCursor( );
}

/* Set the terminal colour */
void
terminal_setColour( enum VgaColour fg, enum VgaColour bg ) {
    _colour = vga_makeColour( fg, bg );
}

/* Write a string of a given length to the screen */
void
terminal_write( const char* data, size_t len ) {
    for( size_t i = 0; i < len && data[i] != '\0'; i++ ) {
        _putchar( data[i] );
    }
    _updateCursor();
}

/* Write a null-terminated string out to the screen and return the number of
characters written */
size_t
terminal_writeString( const char* data ) {
    size_t len = strlen( data );
    terminal_write( data, len );
    return len;
}
