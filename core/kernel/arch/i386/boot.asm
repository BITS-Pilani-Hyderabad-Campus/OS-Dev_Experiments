; #####################################################################
; Entry point of the kernel. The kernel is loaded in 32-bit protected
; mode by the bootloader. It is designed to be multiboot-compliant, and
; to be loaded by a multiboot-compliant bootloader.
;
; The beginning of this file specifies the multiboot header that is
; required at the beginning of the image to tell the bootloader that we
; are multiboot-compliant. It also contains tags which communicate
; different bits of information to the loader (documented as required).
;
; We first check if we're loaded by a multiboot-compliant loader, in
; order to avail of the facilities that multiboot provides. Then we
; check the hardware for its capabilities.
;
; After the end of these checks, we switch to 64-bit long mode and jump
; to the long_mode_init function defined elsewhere, to continue
; execution in long mode.
; #####################################################################

; -------------------------------------------------------------
; The multiboot header
; -------------------------------------------------------------

section .multiboot_header

header_start:
        ; Magic number
        dd      0xE85250D6
        ; Architecture 0 = protected mode i386
        dd      0
        ; Header length
        dd      (header_end - header_start)
        ; Checksum
        dd      (0x100000000 - ((header_end - header_start) + 0 + 0xE85250D6))

        ; Insert optional multiboot2 tags here

        ; Required end tag
        dw      0       ; Type
        dw      0       ; FLags
        dd      8       ; Size
header_end:

; #####################################################################
; The entry point of the kernel
; #####################################################################

section .text
bits    32

global  _start
_start:
        ; Disable interrupts, just in case
        cli
        ; Point ESP to a known valid location
        mov     esp, stack_top
        ; Check if we're loaded by a multiboot2 compliant bootloader
        call    check_multiboot
        ; Initialize the C runtime
        extern  _init
        call    _init
        ; Initialize the environment for the kernel
        extern  kernel_early
        call    kernel_early
        ; Jump to the real kernel. Pass pointer to multiboot info table as
        ; parameter
        push    ebx
        extern  kernel_main
        call    kernel_main

; If we somehow get here, then hang
.hang:
        cli
        hlt
        jmp     $

; -------------------------------------------------------------
; Check if we're loaded by a multiboot2 compliant loader
; -------------------------------------------------------------

check_multiboot:
        ; A multiboot2 compliant bootloader loads this value into EAX
        cmp     eax, 0x36D76289
        jne     .no_multiboot
        ret
.no_multiboot:
        mov     al, '0'
        jmp     error

; -------------------------------------------------------------
; Prints 'ERR: ' to the screen followed by error code in ASCII
; Param:- al = error code in ASCII
; -------------------------------------------------------------

error:
        mov     dword [0xB8000], 0x4F524F45
        mov     dword [0xB8004], 0x4F3A4F52
        mov     dword [0xB8008], 0x4F204F20
        mov     byte [0xB800A], al
        jmp     _start.hang

; #####################################################################
; BSS
; #####################################################################

section .bss
; Align the stack at 0x1000 bytes (4 KB)
align   0x1000
stack_bottom:
        resb    0x1000  ; 4 KiB stack
stack_top:
