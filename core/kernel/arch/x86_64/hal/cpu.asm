; #####################################################################
; Implementation of functions defined in kernel/hal/cpu.h
; Interface for interacting with CPU resources
; #####################################################################

; #####################################################################
; BSS
; #####################################################################

section .bss
vendor: resb 32

; #####################################################################
; Interface function implementation
; #####################################################################

section .text
bits    64

; Initialize the CPU structures
global  cpu_initialize
cpu_initialize:
        extern  idt_initialize
        call    idt_initialize
        ret

; Shut down the CPU
global  cpu_shutdown
cpu_shutdown:
        ret

; Get the CPU vendor string
; Ret :- char* vendor
global  cpu_getVendor
cpu_getVendor:
        push    rbx
        xor     rax, rax        ; CPUID -> get CPU vendor string
        cpuid
        mov     dword [vendor], ebx
        mov     dword [vendor + 4], edx
        mov     dword [vendor + 8], ecx
        mov     rax, vendor
        pop     rbx
        ret
