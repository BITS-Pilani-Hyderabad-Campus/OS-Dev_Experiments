; #####################################################################
; Implementation of the functionality in kernel/hal.h for the x86_64
; platform.
; HAL is the harware abstraction layer, an interface between software
; and hardware
; #####################################################################

; #####################################################################
; External functions
; #####################################################################

extern  cpu_initialize
extern  cpu_shutdown
extern  cpu_getVendor
extern  pic_initialize
extern  pit_initialize
extern  pit_startCounter
extern  pit_getTickCount

; #####################################################################
; Interface function implementation
; #####################################################################

; Initialize the hardware abstraction layer
global  hal_initialize
hal_initialize:
        ; Disable CPU interrupts, just in case
        cli
        ; Initialize the CPU resources
        call    cpu_initialize
        ; Initialize the master and slave PIC and point them to interrupt
        ; vectors 0x20-0x27 and 0x28-0x2F respectively
        mov     rdi, 0x20
        mov     rsi, 0x28
        call    pic_initialize
        ; Initialize thr PIT
        call    pit_initialize
        ; Start the PIT counter with a frequency of 100 MHz, counter 0, square wave gen
        mov     rdi, 100
        mov     rsi, 0x40       ; Counter 0
        mov     rdx, 0x06       ; Square wave generator
        call    pit_startCounter
        ; Enable interrupts
        sti
        ret

; Shut down the hardware abstraction layer
global  hal_shutdown
hal_shuwdown:
        call    cpu_shutdown
        ret

; Write a byte out to a device using memory mapped IO
; Param :- uint16_t port
;          uint8_t data
global  hal_outb
hal_outb:
        mov     rax, rsi
        mov     rdx, rdi
        out     dx, al
        ret

; Read a byte from a device using memory mapped IO
; Param :- uint16_t port
global  hal_inb
hal_inb:
        mov     rdx, rdi
        xor     rax, rax
        in      al, dx
        ret

; Read a word from a device using memory mapped IO
global  hal_inw
hal_inw:
        mov     rdx, rdi
        xor     rax, rax
        in      ax, dx
        ret

; Read a dword from a device using memory mapped IO
global  hal_ind
hal_ind:
        mov     rdx, rdi
        xor     rax, rax
        in      eax, dx
        ret

; Disable interrupts
global  hal_disableInterrupts
hal_disableInterrupts:
        cli
        ret

; Enable interrupts
global  hal_enableInterrupts
hal_enableInterrupts:
        sti
        ret

; Notify the HAL that a device interrupt is done
; Param :- uint8_t irq_num
global  hal_interruptDone
hal_interruptDone:
        ; Check if it's a valid IRQ
        cmp     rdi, 16
        je      .done
        ; If irq_num >= 8, send EOI to both PICs, else send only to master
        mov     al, 0x20        ; EOI signal
        cmp     rdi, 8
        jl      .pic_master
.pic_slave:
        out     0xA0, al
.pic_master:
        out     0x20, al
.done:
        ret

; Get CPU vendor string
global  hal_getCpuVendor
hal_getCpuVendor:
        call    cpu_getVendor
        ret

; Get the tick count from the PIT
global  hal_getTickCount
hal_getTickCount:
        call    pit_getTickCount
        ret
