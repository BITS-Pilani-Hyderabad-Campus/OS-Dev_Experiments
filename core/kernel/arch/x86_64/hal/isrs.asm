; #####################################################################
; Interface for interacting with the Interrupt Descriptor Table and
; also the exception handlers
; #####################################################################

; #####################################################################
; Code
; #####################################################################

section .text
bits    64

; #####################################################################
; Exception handlers
; #####################################################################

; Common exception handler which calls C handler defined in idt.c
; Param :-
;       int_num
;       error_code
_commonHandler:
        ; Store registers
        push    rax
        push    rcx
        push    rdx
;        push    rbx
;        push    rbp
        push    rsi
        push    rdi
        ; Store DS
        xor     rax, rax
        mov     ax, ds
        push    rax
        ; Load kernel data segment
        mov     ax, 0x10
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        ; Call C common ISR handler
        mov     rdi, rsp
        extern  idt_cIsrHandler
        call    idt_cIsrHandler
        ; Restore data segment
        pop     rax
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        ; Restore registers
        pop     rdi
        pop     rsi
;        pop     rbp
;        pop     rbx
        pop     rdx
        pop     rcx
        pop     rax
        ; Clear out error code, false or otherwise
        add     rsp, 8
        ; Return
        iretq

; -------------------------------------------------------------
; Load the IDTR
; Param := pointer_to_istr
; -------------------------------------------------------------

global  isr_loadIdtr
isr_loadIdtr:
        lidt    [rdi]
        ret

; -------------------------------------------------------------
; Macros to make exception handler stubs
; -------------------------------------------------------------

%macro  ISR_NOERRCODE 1
global  isr%1
isr%1:
        push    qword 0         ; False error code
        push    qword %1        ; Push interrupt number
        jmp     _commonHandler
%endmacro

%macro  ISR_ERRCODE 1
global  isr%1
isr%1:
        push    qword %1        ; Push interrupt handler
        jmp     _commonHandler
%endmacro

; -------------------------------------------------------------
; Implementation of the above macros
; -------------------------------------------------------------

ISR_NOERRCODE 0
ISR_NOERRCODE 1
ISR_NOERRCODE 2
ISR_NOERRCODE 3
ISR_NOERRCODE 4
ISR_NOERRCODE 5
ISR_NOERRCODE 6
ISR_NOERRCODE 7
ISR_ERRCODE   8
ISR_NOERRCODE 9
ISR_ERRCODE   10
ISR_ERRCODE   11
ISR_ERRCODE   12
ISR_ERRCODE   13
ISR_ERRCODE   14
ISR_NOERRCODE 15
ISR_NOERRCODE 16
ISR_ERRCODE   17
ISR_NOERRCODE 18
ISR_NOERRCODE 19
ISR_NOERRCODE 20
ISR_NOERRCODE 21
ISR_NOERRCODE 22
ISR_NOERRCODE 23
ISR_NOERRCODE 24
ISR_NOERRCODE 25
ISR_NOERRCODE 26
ISR_NOERRCODE 27
ISR_NOERRCODE 28
ISR_NOERRCODE 29
ISR_ERRCODE   30
ISR_NOERRCODE 31
