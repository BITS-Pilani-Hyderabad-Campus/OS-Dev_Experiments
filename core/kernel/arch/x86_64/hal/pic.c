// #####################################################################
// Implementation of the interface defined in kernel/hal/pic.h
// Interface for interacting with the PIC
// #####################################################################

#include <kernel/hal/pic.h>

// #####################################################################
// Useful definitions
// #####################################################################

//-----------------------------------------------
//	Initialization Command Bit Masks
//-----------------------------------------------

/* Initialization Control Word 1 bit masks */
#define PIC_ICW1_MASK_IC4   0x1     //00000001
#define PIC_ICW1_MASK_SNGL  0x2	    //00000010
#define PIC_ICW1_MASK_ADI   0x4	    //00000100
#define PIC_ICW1_MASK_LTIM  0x8     //00001000
#define PIC_ICW1_MASK_INIT  0x10    //00010000

/* Initialization Control Words 2 and 3 do not require bit masks */

/* Initialization Control Word 4 bit masks */
#define PIC_ICW4_MASK_UPM   0x1	    //00000001
#define PIC_ICW4_MASK_AEOI  0x2	    //00000010
#define PIC_ICW4_MASK_MS    0x4	    //00000100
#define PIC_ICW4_MASK_BUF   0x8	    //00001000
#define PIC_ICW4_MASK_SFNM  0x10    //00010000

//-----------------------------------------------
//	Initialization Command 1 control bits
//-----------------------------------------------

#define PIC_ICW1_IC4_EXPECT             1       //00000001
#define PIC_ICW1_IC4_NO                 0       //00000000
#define PIC_ICW1_SNGL_YES               2       //00000010
#define PIC_ICW1_SNGL_NO                0       //00000000
#define PIC_ICW1_ADI_CALLINTERVAL4      4       //00000100
#define PIC_ICW1_ADI_CALLINTERVAL8      0       //00000000
#define PIC_ICW1_LTIM_LEVELTRIGGERED    8       //00001000
#define PIC_ICW1_LTIM_EDGETRIGGERED     0       //00000000
#define PIC_ICW1_INIT_YES               0x10    //00010000
#define PIC_ICW1_INIT_NO                0       //00000000

//-----------------------------------------------
//	Initialization Command 4 control bits
//-----------------------------------------------

#define PIC_ICW4_UPM_86MODE         1       //00000001
#define PIC_ICW4_UPM_MCSMODE        0       //00000000
#define PIC_ICW4_AEOI_AUTOEOI       2       //00000010
#define PIC_ICW4_AEOI_NOAUTOEOI     0       //00000000
#define PIC_ICW4_MS_BUFFERMASTER    4       //00000100
#define PIC_ICW4_MS_BUFFERSLAVE     0       //00000000
#define PIC_ICW4_BUF_MODEYES        8       //00001000
#define PIC_ICW4_BUF_MODENO         0       //00000000
#define PIC_ICW4_SFNM_NESTEDMODE    0x10    //00010000
#define PIC_ICW4_SFNM_NOTNESTED	    0

// #####################################################################
// Interface function implementation
// #####################################################################

/* Read a data byte from a PIC */
uint8_t
pic_readData( uint8_t pic_num ) {
    if( pic_num > 1 )
        return 0;
    uint16_t reg = ( pic_num == 1 ) ? PIC2_REG_DATA : PIC1_REG_DATA;
    return hal_inb( reg );
}

/* Send a data byte to a PIC */
void
pic_sendData( uint8_t pic_num, uint8_t data ) {
    if( pic_num > 1 )
        return;
    uint16_t reg = ( pic_num == 1 ) ? PIC2_REG_DATA : PIC1_REG_DATA;
    hal_outb( reg, data );
}

/* Send a command to a PIC */
void
pic_sendCommand( uint8_t pic_num, uint8_t command ) {
    if( pic_num > 1 )
        return;
    uint16_t reg = ( pic_num == 1 ) ? PIC2_REG_COMMAND : PIC1_REG_COMMAND;
    hal_outb( reg, command );
}

/* Initialize the PIC */
void
pic_initialize( uint8_t base0, uint8_t base1 ) {
    uint8_t icw	= 0;
	/* disable hardware interrupts */
	hal_disableInterrupts( );
	/* Begin initialization of PIC */
	icw = ( icw & ~PIC_ICW1_MASK_INIT ) | PIC_ICW1_INIT_YES;
	icw = ( icw & ~PIC_ICW1_MASK_IC4 ) | PIC_ICW1_IC4_EXPECT;
	pic_sendCommand( 0, icw );
	pic_sendCommand( 1, icw );
	/* Send initialization control word 2. This is the base addresses of the IRQs */
	pic_sendData ( 0, base0 );
	pic_sendData ( 1, base1 );
	/*
       Send initialization control word 3. This is the connection between master and slave.
       ICW3 for master PIC is the IR that connects to secondary pic in binary format
       ICW3 for secondary PIC is the IR that connects to master pic in decimal format
    */
	pic_sendData ( 0, 0x04 );
	pic_sendData ( 1, 0x02 );
	/* Send Initialization control word 4. Enables i86 mode */
	icw = ( icw & ~PIC_ICW4_MASK_UPM ) | PIC_ICW4_UPM_86MODE;
	pic_sendData( 0, icw );
	pic_sendData( 1, icw );
}
