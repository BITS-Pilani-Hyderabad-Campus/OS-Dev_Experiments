// #####################################################################
// Interface for interacting with the Interrupt Descriptor Table and
// also the exception handlers
// #####################################################################

#include <kernel/hal/idt.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// #####################################################################
// Internal structures
// #####################################################################

/* The structure of an IDT descriptor */
struct GateDesc {
    uint16_t    base_low;
    uint16_t    selector;
    uint16_t    flags;
    /*
    bits 0-2    =>  0: Don't switch stacks
                    7: Switch to the nth stack in the interrupt stack table
    bits 3-7    =>  Reserved
    bit  8      =>  0: Interrupt gate
                    1: Trap gate
    bits 9-11   =>  must be 1
    bit  12     =>  must be 0
    bits 13-14  =>  DPL
    bit  15     =>  Present
    */
    uint16_t    base_middle;
    uint32_t    base_high;
    uint32_t    reserved;
} __attribute__((packed));

/* The structure of the IDTR */
struct Idtr {
    uint16_t    limit;
    uint64_t    base;
} __attribute__((packed));

/* Registers pushed to stack as a block of memory, pointer to which is passed
to the common ISR handler */
struct Regs {
    /* The data segment of the code that was being executed before the interrupt */
    uint64_t    ds;
    /* General purpose registers */
    uint64_t    rdi;
    uint64_t    rsi;
    uint64_t    rbp;
    uint64_t    rbx;
    uint64_t    rdx;
    uint64_t    rcx;
    uint64_t    rax;
    /* Interrupt number */
    uint64_t    int_num;
    /* Error code */
    uint64_t    err_code;
    /* RIP, CS, EFLAGS, USER_ESP ( if requried ), USER_SS (if required ) */
    uint64_t    rip;
    uint64_t    cs;
    uint64_t    eflags;
    uint64_t    user_esp;
    uint64_t    ss;
} __attribute__((packed));

// #####################################################################
// Useful constants
// #####################################################################

#define MAX_INT_NUM 256

// #####################################################################
// External functions
// #####################################################################

// -------------------------------------------------------------
// ISRS
// -------------------------------------------------------------

extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();

// -------------------------------------------------------------
// Other functions
// -------------------------------------------------------------

extern void
isr_loadIdtr( void* idtr_ptr );

// #####################################################################
// Internal data members
// #####################################################################

static struct GateDesc* _gate_desc_arr;
static struct Idtr _idtr;

// #####################################################################
// Implementation functions
// #####################################################################

// -------------------------------------------------------------
// Bit-twiddling functions for IDT dest flags
// -------------------------------------------------------------

static inline void
_setMinimal( struct GateDesc* desc ) {
    desc->flags &= ~( 0x1F << 3 );     /* Clear reserved bits */
    desc->flags |= ( 0x07 << 9 );      /* Set bit 9 */
    desc->flags &= ~( 0x01 << 12 );    /* Clear bits 10-12 */
}

static inline void
_setPresent( struct GateDesc* desc ) {
    desc->flags |= ( 1 << 15 );
}

static inline void
_setAbsent( struct GateDesc* desc ) {
    desc->flags &= ~( 1 << 15 );
}

static inline void
_setInterruptGate( struct GateDesc* desc ) {
    desc->flags &= ~( 1 << 8 );
}

static inline void
_setTrapGate( struct GateDesc* desc ) {
    desc->flags |= ( 1 << 8 );
}

static inline void
_setDpl( struct GateDesc* desc, uint8_t dpl ) {
    desc->flags &= ~( 3 << 13 );
    desc->flags |= ( ( dpl << 13 ) & 0x03 );
}

static inline void
_setStackIndex( struct GateDesc* desc, uint8_t index ) {
    desc->flags &= ~3;
    desc->flags |= ( index & 0x03 );
}

// -------------------------------------------------------------
// Default exception handler
// -------------------------------------------------------------

static void
_defaultHandler( ) {
    printf( "Unhandler interrupt! Aborting...\n" );
    abort( );
}

// -------------------------------------------------------------
// Create new IDT entry
// -------------------------------------------------------------

static void
_setDescriptor( uint8_t index, uint64_t base_addr, uint16_t selector, uint8_t dpl ) {
    _gate_desc_arr[index].base_low = ( uint16_t ) base_addr & 0xFFFF;
    _gate_desc_arr[index].base_middle = ( uint16_t ) ( base_addr >> 16 ) & 0xFFFF;
    _gate_desc_arr[index].base_high = ( uint32_t ) ( base_addr >> 48 ) & 0xFFFFFFFF;

    _gate_desc_arr[index].selector = selector;

    _setMinimal( &_gate_desc_arr[index] );
    _setPresent( &_gate_desc_arr[index] );
    _setInterruptGate( &_gate_desc_arr[index] );
    _setDpl( &_gate_desc_arr[index], dpl );
    _setStackIndex( &_gate_desc_arr[index], 0 );

    _gate_desc_arr[index].reserved = 0;
}

// #####################################################################
// Common C interrupt handler
// #####################################################################

void
idt_cIsrHandler( struct Regs* regs ) {
    printf( "Interrupt number: %d\n", regs->int_num );
    abort( );
}

// #####################################################################
// Interface function implementation
// #####################################################################

/* Register interrupt handler */
void
idt_registerHandler( uint8_t int_num, void* base_addr ) {
    _setDescriptor( int_num, ( uint64_t ) base_addr, 0x08, 0 );
}


/* Initialize the IDT */
/* Make sure GCC doesn't optimize this away */
__attribute__((optimize("O0")))
void
idt_initialize( ) {
    /* Set base of gate_desc_arr at 0 */
    _gate_desc_arr = 0;
    /* Set the base and limit of the IDTR */
    _idtr.limit = ( 256 * sizeof( struct GateDesc ) ) - 1;
    _idtr.base = 0;
    /* Set the specific exception handlers */
    _setDescriptor( 0, ( uint64_t ) isr0, 0x08, 0 );
    _setDescriptor( 1, ( uint64_t ) isr1, 0x08, 0 );
    _setDescriptor( 2, ( uint64_t ) isr2, 0x08, 0 );
    _setDescriptor( 3, ( uint64_t ) isr3, 0x08, 0 );
    _setDescriptor( 4, ( uint64_t ) isr4, 0x08, 0 );
    _setDescriptor( 5, ( uint64_t ) isr5, 0x08, 0 );
    _setDescriptor( 6, ( uint64_t ) isr6, 0x08, 0 );
    _setDescriptor( 7, ( uint64_t ) isr7, 0x08, 0 );
    _setDescriptor( 8, ( uint64_t ) isr8, 0x08, 0 );
    _setDescriptor( 9, ( uint64_t ) isr9, 0x08, 0 );
    _setDescriptor( 10, ( uint64_t ) isr10, 0x08, 0 );
    _setDescriptor( 11, ( uint64_t ) isr11, 0x08, 0 );
    _setDescriptor( 12, ( uint64_t ) isr12, 0x08, 0 );
    _setDescriptor( 13, ( uint64_t ) isr13, 0x08, 0 );
    _setDescriptor( 14, ( uint64_t ) isr14, 0x08, 0 );
    _setDescriptor( 15, ( uint64_t ) isr15, 0x08, 0 );
    _setDescriptor( 16, ( uint64_t ) isr16, 0x08, 0 );
    _setDescriptor( 17, ( uint64_t ) isr17, 0x08, 0 );
    _setDescriptor( 18, ( uint64_t ) isr18, 0x08, 0 );
    _setDescriptor( 19, ( uint64_t ) isr19, 0x08, 0 );
    _setDescriptor( 20, ( uint64_t ) isr20, 0x08, 0 );
    _setDescriptor( 21, ( uint64_t ) isr21, 0x08, 0 );
    _setDescriptor( 22, ( uint64_t ) isr22, 0x08, 0 );
    _setDescriptor( 23, ( uint64_t ) isr23, 0x08, 0 );
    _setDescriptor( 24, ( uint64_t ) isr24, 0x08, 0 );
    _setDescriptor( 25, ( uint64_t ) isr25, 0x08, 0 );
    _setDescriptor( 26, ( uint64_t ) isr26, 0x08, 0 );
    _setDescriptor( 27, ( uint64_t ) isr27, 0x08, 0 );
    _setDescriptor( 28, ( uint64_t ) isr28, 0x08, 0 );
    _setDescriptor( 29, ( uint64_t ) isr29, 0x08, 0 );
    _setDescriptor( 30, ( uint64_t ) isr30, 0x08, 0 );
    _setDescriptor( 31, ( uint64_t ) isr31, 0x08, 0 );
    /* Set the default handler for the rest */
    for( int int_num = 32; int_num < MAX_INT_NUM; int_num++ ) {
        _setDescriptor( ( uint8_t ) int_num, ( uint64_t ) _defaultHandler, 0x08, 0 );
    }
    /* Load the IDTR */
    isr_loadIdtr( &_idtr );
}
