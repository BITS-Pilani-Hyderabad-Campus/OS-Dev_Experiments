; #####################################################################
; Implementation of some of the functions defined in kernel/hal/pit.h
; #####################################################################

; #####################################################################
; Data
; #####################################################################

section .text
_tick_count:     dq 0

; #####################################################################
; Code
; #####################################################################

section .text
bits    64

; -------------------------------------------------------------
; The PIT IRQ
; -------------------------------------------------------------

_pitIrq:
        push    rax
        inc     qword [_tick_count]
        ; Send EOI to PIC 0
        mov     al, 0x20
        out     0x20, al
        pop     rax
        iretq


; -------------------------------------------------------------
; Set the new PIT tick count and return the old tick count
; PARAM :-
;       size_t new_tick_count
;RET    :-
;       size_t old_tick_count
; -------------------------------------------------------------

global  pit_setTickCount
pit_setTickCount:
        mov     rax, qword [_tick_count]
        mov     qword [_tick_count], rdi
        ret

; -------------------------------------------------------------
; Returns the current PIT tick count
; PARAM :-
; RET   :-
;       size_t tick_count
; -------------------------------------------------------------

global  pit_getTickCount
pit_getTickCount:
        mov     rax, qword [_tick_count]
        ret

; -------------------------------------------------------------
; Start the PIT counter
; PARAM :-
;       uint32_t frequency
;       uint8_t  counter
;       uint8_t  mode
; RET   :-
; -------------------------------------------------------------

global  pit_startCounter
pit_startCounter:
        ; Store registers
        push    rbx
        push    rdx
        ; Some sanity checks
        mov     eax, 0x10000    ; EAX = reload value for slowest possible frequency (65536)
        cmp     edi, 18         ; Is the requested frequency too low?
        jbe     .got_reload_val ; Yes, use slowest possible frequency
        mov     eax, 1          ; EAX = reload value for fastest possible frequency (1)
        cmp     edi, 1193181    ; Is the requested frequency too high?
        jae     .got_reload_val ; Yes, use fastest possible frequency

        ; Calculate the reload value
        mov     rax, 1193181
        xor     rdx, rdx
        div     rdi
        cmp     rdx, 1193181 / 2
        jl      .okay
.round_up:
        inc     rax
.okay:
.got_reload_val:
        mov     rbx, rax
        ; Send the OCW to the PIT
        xor     rax, rax
        pop     rdx             ; Mode
        or      rax, rdx
        or      rax, 0x30       ; OCW_RL_DATA
        or      rax, rsi        ; Counter
        ; Do the actual sending
        out     0x43, al
        ; Set frequency rate
        mov     rax, rbx
        ; Send low byte to counter 0
        out     0x40, al
        ; Send the high byte to counter 0
        shr     ax, 8
        out     0x40, al
        ; Reset the tick count
        mov     qword [_tick_count], 0
        ; Restore RBX
        pop     rbx
        ret

; -------------------------------------------------------------
; Initialize the PIT
; -------------------------------------------------------------

global  pit_initialize
pit_initialize:
        mov     rdi, 32
        mov     rsi, _pitIrq
        extern  idt_registerHandler
        call    idt_registerHandler
        ret
