; #####################################################################
; Entry point of the kernel. The kernel is loaded in 32-bit protected
; mode by the bootloader. It is designed to be multiboot-compliant, and
; to be loaded by a multiboot-compliant bootloader.
;
; The beginning of this file specifies the multiboot header that is
; required at the beginning of the image to tell the bootloader that we
; are multiboot-compliant. It also contains tags which communicate
; different bits of information to the loader (documented as required).
;
; We first check if we're loaded by a multiboot-compliant loader, in
; order to avail of the facilities that multiboot provides. Then we
; check the hardware for its capabilities.
; We require that the hardware support -
; CPUID
; Long mode
;
; After the end of these checks, we switch to 64-bit long mode and jump
; to the long_mode_init function defined elsewhere, to continue
; execution in long mode.
; #####################################################################

; -------------------------------------------------------------
; The multiboot header
; -------------------------------------------------------------

section .multiboot_header

header_start:
        ; Magic number
        dd      0xE85250D6
        ; Architecture 0 = protected mode i386
        dd      0
        ; Header length
        dd      (header_end - header_start)
        ; Checksum
        dd      (0x100000000 - ((header_end - header_start) + 0xE85250D6))

        ; Insert optional multiboot2 tags here

        ; Required end tag
        dw      0       ; Type
        dw      0       ; FLags
        dd      8       ; Size
header_end:

; #####################################################################
; The entry point of the kernel
; #####################################################################

section .text
bits    32

global  _start
_start:
        ; Disable interrupts, just in case
        cli
        ; Point ESP to a known valid location
        mov     esp, stack_top
        ; Check if we're loaded by a multiboot2 compliant bootloader
        call    check_multiboot
        ; Store EBX which contains the pointer to the multiboot header table
        push    ebx
        ; Check if CPUID is supported
        call    check_cpuid
        ; Check if Long Mode is supported
        call    check_long_mode
        ; Set up the initial page table structure
        call    set_up_page_tables
        ; Enable paging
        call    enable_paging
        ; Enable SSE
        ; call    set_up_SSE
        ; Restore EBX, which will be passed to long_mode_init
        pop     ebx
        ; Switch to a 64-bit GDT
        lgdt    [gdtr64]
        ; Switch to valid data segments
        mov     ax, gdt64.data
        mov     ds, ax
        mov     es, ax
        mov     ss, ax
        ; Fix CS using a 64-bit far jump
        extern  long_mode_init
        jmp     gdt64.code:long_mode_init
; If we somehow get here, then hang
.hang:
        cli
        hlt
        jmp     $

; -------------------------------------------------------------
; Check if we're loaded by a multiboot2 compliant loader
; -------------------------------------------------------------

check_multiboot:
        ; A multiboot2 compliant bootloader loads this value into EAX
        cmp     eax, 0x36D76289
        jne     .no_multiboot
        ret
.no_multiboot:
        mov     al, '0'
        jmp     error

; -------------------------------------------------------------
; Check if CPUID is available
; -------------------------------------------------------------

check_cpuid:
        ; Check if CPUID is supported by attempting to flip the ID bit (bit 21)
        ; in the FLAGS register. If we can flip it, CPUID is available.
        pushfd
        pop     eax
        mov     ecx, eax
        xor     eax, 1 << 21
        push    eax
        popfd
        pushfd
        pop     eax
        push    ecx
        popfd
        or      eax, ecx
        jz      .no_cpuid
        ret
.no_cpuid:
        mov     al, '1'
        jmp     error

; -------------------------------------------------------------
; Check if Long Mode is available
; -------------------------------------------------------------

check_long_mode:
        ; Test if extended processor info is available
        mov     eax, 0x80000000 ; Get highest supported argument
        cpuid
        cmp     eax, 0x80000001 ; Needs to atleast be this
        jb      .no_long_mode
        ; Use extended processor info to check if long mode is
        ; available
        mov     eax, 0x80000001
        cpuid
        test    edx, 1 << 29    ; Test if the Long Mode bit is set
        jz      .no_long_mode
        ret
.no_long_mode:
        mov     al, '2'
        jmp     error

; -------------------------------------------------------------
; Set up page tables. PML4 at 0x1000, PDP at 0x2000, PD at 0x3000
; -------------------------------------------------------------

; Identity-map the first 1GB of memory.
; The tables will be set up at :
;       PML4    -> 0x1000
;       PDP     -> 0x2000
;       PD      -> 0x3000
set_up_page_tables:
        ; Clear out space from 0x1000 to 0x4000 (0x3000 bytes)
        mov     edi, 0x1000
        xor     eax, eax
        mov     ecx, 0x3000 >> 2 ; Since we can store DWORDS
        cld
        rep     stosd
        ; Map first PML4 entry to base of PDP
        mov     eax, 0x2000
        or      eax, 0x03       ; Present + writable
        mov     dword [0x1000], eax
        ; Map first PDP entry to base of PD
        mov     eax, 0x3000
        or      eax, 0x03       ; Present + writable
        mov     dword [0x2000], eax
        ; Map each PD entry to a huge 2MiB page
        mov     edi, 0x3000
        xor     eax, eax
        mov     eax, 0x83       ; Present + writable + huge
        mov     ecx, 512
        .loop:
                stosd
                add     edi, 4
                loop    .loop
        ret

; -------------------------------------------------------------
; Enable paging
; -------------------------------------------------------------

enable_paging:
        ; Load PML4 to CR3
        mov     eax, 0x1000
        mov     cr3, eax
        ; Enable Physical Address Extension flag in CR4
        mov     eax, cr4
        or      eax, 1 << 5
        mov     cr4, eax
        ; Set the long mode bit in the EFER MSR
        mov     ecx, 0xC0000080
        rdmsr
        or      eax, 1 << 8
        wrmsr
        ; Enable paging in the CR0 register
        mov     eax, cr0
        or      eax, 1 << 31
        mov     cr0, eax
        ret

; -------------------------------------------------------------
; Check for SSE and enable it. If it is not supported, throw the
; error '4'
; -------------------------------------------------------------
;set_up_SSE:
;        ; check for SSE
;        mov     eax, 0x1
;        cpuid
;        test    edx, 1<<25
;        jz      .no_SSE
;        ; enable SSE
;        mov     eax, cr0
;        and     ax, 0xFFFB      ; Clear coprocessor emulation CR0.EM
;        or      ax, 0x2         ; Set coprocessor monitoring  CR0.MP
;        mov     cr0, eax
;        mov     eax, cr4
;        or      ax, 3 << 9      ; Set CR4.OSFXSR and CR4.OSXMMEXCPT
;        mov     cr4, eax
;        ret
;.no_SSE:
;        mov     al, '4'
;        jmp     error

; -------------------------------------------------------------
; Prints 'ERR: ' to the screen followed by error code in ASCII
; Param:- al = error code in ASCII
; -------------------------------------------------------------

error:
        mov     dword [0xB8000], 0x4F524F45
        mov     dword [0xB8004], 0x4F3A4F52
        mov     dword [0xB8008], 0x4F204F20
        mov     byte [0xB800A], al
        jmp     _start.hang

; #####################################################################
; BSS
; #####################################################################

section .bss
; Align the stack at 0x1000 bytes (4 KB)
align   0x1000
stack_bottom:
        resb    0x1000  ; 4 KiB stack
stack_top:

; #####################################################################
; RODATA
; #####################################################################

section .rodata

; The 64-bit GDT
gdt64:
.null:  equ     $ - gdt64
        dq      0
.code:  equ     $ - gdt64
        dq      (1<<44) | (1<<47) | (1<<41) | (1<<43) | (1<<53)
.data:  equ     $ - gdt64
        dq      (1<<44) | (1<<47) | (1<<41)
.end:

gdtr64:
        .limit: dw (gdt64.end - gdt64) - 1
        .base:  dq gdt64
