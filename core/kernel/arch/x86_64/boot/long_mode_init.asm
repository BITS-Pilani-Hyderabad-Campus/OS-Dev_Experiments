global  long_mode_init

section .text
bits 64
long_mode_init:
        ; Initialize the C runtime
        extern  _init
        call    _init
	; Send pointer to multiboot header table as a parameter to kernel_early
	xor	rdi, rdi
	mov	edi, ebx
        extern  kernel_early
        call    kernel_early
        ; Call the main kernel
	extern	kernel_main
	call	kernel_main
        ; print `OKAY` to screen
        ;mov     rax, 0x2F592F412F4b2F4F
        ;mov     qword [0xB8000], rax
	cli
	hlt
