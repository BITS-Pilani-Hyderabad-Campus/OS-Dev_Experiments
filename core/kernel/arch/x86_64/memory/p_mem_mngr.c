// #####################################################################
// This is the part of the memory module that we'll use to allocate
// frames of physical memory when required.
// This is now very basic, and I plan to improve the algorithms greatly
// in order to promote scalability. Or even basic sense.
// That's why the goal is to make this easily plug-in-able, and the
// interface neutral of the implementation, so that I can upgrade the
// implementation later without breaking anything.
//
// This file contains the implementation.
//
// The memory manager is implemented as a bitmap, where each bit represents
// a 4096B (4KB) block of memory. If a bit is set, the corresponding
// block has been allocated. If a bit is unset, the corresponding block
// is free.
// #####################################################################

#include <kernel/memory/p_mem_mngr.h>

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

// #####################################################################
// Internal private variables and definitions
// #####################################################################

// 8 bits per byte -> 8 blocks per byte
#define P_MEM_BLOCKS_PER_BYTE   8
// Block size
#define P_MEM_BLOCK_SIZE    0x1000  // 4 KB
// Block alignment
#define P_MEM_BLOCK_ALIGN   0x1000

// Size of physical memory
static size_t       _phys_mem_size = 0;
// Number of blocks currently in use
static size_t       _num_used_blocks = 0;
// Maximum number of available blocks
static size_t       _num_max_blocks = 0;
// Memory bit map array. Each bit represents a memory block.
static uint64_t*    _mem_map = 0;

// #####################################################################
// Internal functions
// #####################################################################

/* Set the given bit in the bitmap */
static inline void
_memMapSet( size_t bit ) {
    if( bit < _num_max_blocks )
        _mem_map[bit / 64] |= ( 1 << ( bit % 64 ) );
}


/* Unset the given bit in the bitmap */
static inline void
_memMapUnset( size_t bit ) {
    if( bit < _num_max_blocks )
        _mem_map[bit / 64] &= ~( 1 << ( bit % 64 ) );
}

/* Test if a given bit is set */
static inline bool
_memMapTest( size_t bit ) {
    if( bit >= _num_max_blocks )
        return false;
    return _mem_map[bit / 64] & ( 1 << ( bit % 64 ) );
}

/* Find the first free frame in the bit array and return its index */
static int
_memMapFirstFree( ) {
    for( size_t i = 0; i < _num_max_blocks / 64; i++ ) {
        if( _mem_map[i] != 0xFFFFFFFFFFFFFFFF ) {
            for( size_t j = 0; j < 64; j++ ) {
                if( !( _mem_map[i] & ( 1 << j ) ) )
                    return ( i * 64 ) + j;
            }
        }
    }
    return -1;
}

/* Finds the first string of n free blocks and returns the index of the first one */
static int
_memMapFirstNFree( size_t n ) {
    if( n == 0 )
        return -1;
    if( n == 1 )
        return _memMapFirstFree( );
    for( size_t i = 0; i < _num_max_blocks / 64; i++ ) {
        if( _mem_map[i] != 0xFFFFFFFFFFFFFFFF ) {
            for( size_t j = 0; j < 64; j++ ) {
                if( !( _mem_map[i] & ( 1 << j ) ) ) {
                    size_t start_bit = ( i * 64 ) + j;
                    size_t free = 1;
                    // Loop through each next bit to see if there is enough space
                    for( size_t count = 1; count < n; count++ ) {
                        if( !_memMapTest( start_bit + count ) )
                            free++;
                        if( free == n )
                            return ( i * 64 ) + j;
                    }
                }
            }
        }
    }
    return -1;
}

// #####################################################################
// Interface function implementation
// #####################################################################

/* Initialize the physical memory manager. Set up variables */
void
pMemMngr_init( size_t memory_size, void* bitmap_addr ) {
    _phys_mem_size = memory_size;
    _mem_map = ( uint64_t* ) bitmap_addr;
    _num_max_blocks = _phys_mem_size / P_MEM_BLOCK_SIZE;
    _num_used_blocks = _num_max_blocks;

    /* By default, all of memory is in use */
    memset( _mem_map, 0xFF, _num_max_blocks / P_MEM_BLOCKS_PER_BYTE );
}

/* Enables a stretch of physical memory for use */
void
pMemMngr_initRegion( void* base_addr, size_t len ) {
    size_t align = ( size_t ) base_addr / P_MEM_BLOCK_SIZE;
    size_t blocks = len / P_MEM_BLOCK_SIZE;

    for( ; blocks > 0; blocks-- ) {
        _memMapUnset( align++ );
        _num_used_blocks--;
    }
}

/* Disables a stretch of physical memory for use */
void
pMemMngr_deinitRegion( void* base_addr, size_t len ) {
    size_t align = ( size_t ) base_addr / P_MEM_BLOCK_SIZE;
    size_t blocks = len / P_MEM_BLOCK_SIZE;
    if( len % P_MEM_BLOCK_SIZE != 0 )
        blocks++;

    for( ; blocks > 0; blocks-- ) {
        _memMapSet( align++ );
        _num_used_blocks++;
    }
}

/* Allocates a single memory block and returns its base address */
void*
pMemMngr_alloc() {
    /* Check if we're out of memory */
    if( _num_max_blocks - _num_used_blocks <= 0 )
        return 0;
    /* Get frame index */
    int frame = _memMapFirstFree( );
    /* Check if we're out of memory */
    if( frame == -1 )
        return 0;
    /* Mark frame as used and return its physical address */
    _memMapSet( frame );
    void* addr = ( void* ) ( ( size_t ) frame * P_MEM_BLOCK_SIZE );
    _num_used_blocks++;
    return addr;
}

/* Allocates n contiguous blocks of memory */
void*
pMemMngr_allocN( size_t n ) {
    /* Check if we're out of memory */
    if( _num_max_blocks - _num_used_blocks < n )
        return 0;
    /* Get frame index */
    int frame = _memMapFirstNFree( n );
    /* Check if we're out of memory */
    if( frame == -1 )
        return 0;
    /* Mark frames as used and return its physical address */
    for( size_t i = 0; i < n; i++ )
        _memMapSet( frame + i );
    void* addr = ( void* ) ( ( size_t ) frame * P_MEM_BLOCK_SIZE );
    _num_used_blocks++;
    return addr;
}

/* Deallocates (frees) a single memory block given base address */
void
pMemMngrFree( void* base_addr ) {
    size_t frame = ( size_t ) base_addr / P_MEM_BLOCK_SIZE;
    _memMapUnset( frame );
    _num_used_blocks--;
}

/* Deallocates n contiguous blocks given the base address and n */
void
pMemMngr_freeN( void* base_addr, size_t n ) {
    size_t frame = ( size_t ) base_addr / P_MEM_BLOCK_SIZE;
    for( size_t i = 0; i < n; i++ )
        _memMapUnset( frame + i );
    _num_used_blocks -= n;
}

/* Get the total size of physical memory in bytes */
size_t
pMemMngr_getMemorySize( ) {
    return _phys_mem_size;
}

/* Get the total number of memory blocks */
size_t
pMemMngr_getBlockCount( ) {
    return _num_max_blocks;
}

/* Get the number of used blocks */
size_t
pMemMngr_getUsedBlockCount( ) {
    return _num_used_blocks;
}

/* Get the number of free blocks */
size_t
pMemMngr_getFreeBlockCount( ) {
    return _num_max_blocks - _num_used_blocks;
}

/* Get the size of one block */
size_t
pMemMngr_getBlockSize( ) {
    return P_MEM_BLOCK_SIZE;
}
