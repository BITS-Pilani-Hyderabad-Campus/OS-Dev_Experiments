// #####################################################################
// Interface for retrieving information from the multiboot information
// table passed to the kernel by the bootloader
// #####################################################################

#include <kernel/multiboot.h>
#include <stddef.h>

// #####################################################################
// Interface function implementation
// #####################################################################

// -------------------------------------------------------------
// General tags
// -------------------------------------------------------------

/* Get address of the next tag. NULL if not present */
struct Tag*
multiboot_getNextTag( const struct Tag* tag ) {
    struct Tag* next = ( struct Tag* ) tag;
    size_t next_off = next->size;
    if( next_off % 8 != 0 )
        next_off = ( next_off & ~( 0x07 ) ) + 8;
    void* v_next = ( void* ) next;
    v_next += next_off;
    next = ( struct Tag* ) v_next;
    return next;
}

/* Get address of tag given type. NULL if not present */
struct Tag*
multiboot_getTagType ( const struct BootInfoFixed* table, uint32_t type ) {
    struct Tag* tag = multiboot_getFirstTag( table );
    while( tag->type != 0 ) {
        if( tag->type == type )
            return tag;
        tag = multiboot_getNextTag( tag );
    }
    return 0;
}

// -------------------------------------------------------------
// Memory map tag
// -------------------------------------------------------------

/* Get address of first memory area entry */
struct MemoryArea*
multiboot_getFirstMemArea( const struct MemoryMapTag* memory_map_tag ) {
    void* tag_addr = ( void* ) memory_map_tag;
    size_t size = sizeof( struct MemoryMapTag );
    tag_addr += size;
    return ( struct MemoryArea* ) tag_addr;
}

/* Get address of next memory area. Return NULL if not available */
struct MemoryArea*
multiboot_getNextMemArea( const struct MemoryMapTag* memory_map_tag, const struct MemoryArea* memory_area ) {
    /* First check if we're done with memory map tags */
    void* last_addr = ( void* ) memory_map_tag;
    last_addr += memory_map_tag->size;
    void* entry_addr = ( void* ) memory_area;
    entry_addr += memory_map_tag->entry_size;
    if( entry_addr >= last_addr ) {
        return 0;
    }
    /* Check for a few bugs */
    struct MemoryArea* return_val = ( struct MemoryArea* ) entry_addr;
    if( return_val->base == 0 )
        return 0;
    return return_val;
}

// -------------------------------------------------------------
// ELF section tag
// -------------------------------------------------------------

/* Get address of ELF section given section's index in table */
struct ElfSection*
multiboot_getElfSectionNum( const struct ElfSectionsTag* elf_section_tag, uint32_t elf_section_num ) {
    if( elf_section_num >= elf_section_tag->num_sections ) {
        return 0;
    }
    void* addr = ( void* ) elf_section_tag;
    addr += sizeof( struct ElfSectionsTag );
    addr += sizeof( struct ElfSection ) * elf_section_num;
    return ( struct ElfSection* ) addr;
}
