// #####################################################################
// C library -> String manipulation functions
// #####################################################################

#ifndef _STRING_H_
#define _STRING_H_  1

#include <sys/cdefs.h>
#include <stddef.h>

/* Fix C++ name-mangling */
#ifdef __cplusplus
extern "C" {
#endif

// #####################################################################
// Interface functions
// #####################################################################

// -------------------------------------------------------------
// Compare given byte of memory at the two given location
// PARAM :-
//  const void* a_ptr
//  const void* b_ptr
//  size_t number_of_bytes
// RET   :-
//  int ( 0 if equal, -1 if a is smaller, 1 is a is larger )
// -------------------------------------------------------------

int
memcmp( const void* a_ptr, const void* b_ptr, size_t n );

// -------------------------------------------------------------
// Move the given number of bytes of memory from src to dest.
// Doesn't check for overlapping memory. Returns a pointer to dest
// PARAM :-
//  void* dest
//  const void* src
//  size_t number_of_bytes
// RET   :-
//  void*   dest
// -------------------------------------------------------------

void*
memcpy( void* __restrict dest, const void* __restrict src, size_t n );

// -------------------------------------------------------------
// Move the given number of bytes of memory from src to dest.
// Checks for overlapping memory. Returns a pointer to dest
// PARAM :-
//  void* dest
//  const void* src
//  size_t number_of_bytes
// RET   :-
//  void*   dest
// -------------------------------------------------------------

void*
memmove( void* dest, const void* src, size_t n );

// -------------------------------------------------------------
// Store the given byte value at the specified number of locations
// after th given location
// PARAM :-
//  void* dest
//  int value
//  size_t number_of_bytes
// RET   :-
//  void* dest
// -------------------------------------------------------------

void*
memset( void* dest, int val, size_t n );

// -------------------------------------------------------------
// Return the length in bytes of a null terminated string
// PARAM :-
//  const char* str
// RET   :-
//  size_t length
// -------------------------------------------------------------

size_t
strlen( const char* str );

#ifdef __cplusplus
}
#endif

#endif
