// #####################################################################
// C library -> Standard input/output functions.
// #####################################################################

#ifndef _STDIO_H_
#define _STDIO_H_ 1

#include <sys/cdefs.h>

/* Fix C++ name-mangling */
#ifdef __cplusplus
extern "C" {
#endif

// #####################################################################
// Interface functions
// #####################################################################

// -------------------------------------------------------------
// Print a single character to the screen, and return the ASCII
// value of the character
// PARAM :-
//  int character_to_print
// RET   :-
//  int ascii_value_of_character
// -------------------------------------------------------------

int
putchar( int ci );

// -------------------------------------------------------------
// Prints a null-terminated string to screen, followed by a
// newline. Returns the number of characters printed.
// PARAM :-
//  const char* null_terminated_string
// RET   :-
//  int number_of_chars_printed
// -------------------------------------------------------------

int
puts( const char* str );

// -------------------------------------------------------------
// Print formatted output to the screen. Return the number of
// characters printed
// PARAM :-
//  const char* format
//  variable number of arguments
// RET   :-
//  int number_of_chars_printed
// -------------------------------------------------------------

int
printf( const char* __restrict format, ... );

#ifdef __cplusplus
}
#endif

#endif
