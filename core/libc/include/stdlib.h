// #####################################################################
// C library -> Standard library functions
// #####################################################################

#ifndef _STDLIB_H_
#define _STDLIB_H_  1

#include <sys/cdefs.h>

/* Fix C++ name-mangling */
#ifdef __cplusplus
extern "C" {
#endif

// #####################################################################
// Interface functions
// #####################################################################

// -------------------------------------------------------------
// Abort kernel execution
// PARAM :-
// RET   :-
// -------------------------------------------------------------

__attribute__((__noreturn__))
void
abort( );

// -------------------------------------------------------------
// Convert a signed integer into its equivalent string
// representation. Write to given buffer and return a pointer to
// that buffer
// PARAM :-
//  int value
//  char* buffer
//  unsigned int base
// RET   :-
//  char* buffer
// -------------------------------------------------------------

char*
itoa( long val, char* buffer, unsigned long base );

// -------------------------------------------------------------
// Convert an unsigned integer into its equivalent string
// representation. Write to given buffer and return a pointer to
// that buffer
// PARAM :-
//  unsigned int value
//  char* buffer
//  unsigned int base
// RET   :-
//  char* buffer
// -------------------------------------------------------------

char*
utoa( unsigned long val, char* buffer, unsigned long base );

#ifdef __cplusplus
}
#endif

#endif
