// #####################################################################
// Implementation of the puts function defined in stdio.h
// Prints a null-terminated string to screen, followed by a newline
// #####################################################################

#include <stdio.h>

/* Check if the code is part of the kernel. If yes, we can directly use the
tty routines. If not, we would need to use the write-string system call */
#if defined(__is_os1_kernel)
#include <kernel/tty.h>
#endif

// -------------------------------------------------------------
// Prints a null-terminated string to screen, followed by a
// newline. Returns the number of characters printed.
// PARAM :-
//  const char* null_terminated_string
// RET   :-
//  int number_of_chars_printed
// -------------------------------------------------------------

int
puts( const char* str ) {
    int count = 0;
#if defined(__is_os1_kernel)
    count = terminal_writeString( str ) + 1;
    terminal_write( "\n", 1 );
#else
    /* TODO: Implement sys_write system call */
#endif
    return count;
}
