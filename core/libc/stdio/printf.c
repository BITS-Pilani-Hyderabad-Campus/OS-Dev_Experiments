// #####################################################################
// Implementation of the printf function defined in stdio.h
// #####################################################################

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>

/* Check if the code is part of the kernel. If yes, we can directly use the
tty routines. If not, we would need to use the write-string system call */
#if defined(__is_os1_kernel)
#include <kernel/tty.h>
#endif

// -------------------------------------------------------------
// Internal function to ease out printing
// PARAM :-
//  const char* string
//  size_t      number_of_bytes_to_print
// RET   :-
// -------------------------------------------------------------

static inline void
print( const char* str, size_t len ) {
#if defined(__is_os1_kernel)
    terminal_write( str, len );
#else
    // TODO: implement sys_write system call
#endif
}

// -------------------------------------------------------------
// Print formatted output to the screen. Return the number of
// characters printed
// PARAM :-
//  const char* format
//  variable number of arguments
// RET   :-
//  int number_of_chars_printed
// -------------------------------------------------------------

int
printf( const char* restrict format, ... ) {
    va_list parameters;
    va_start( parameters, format );

    int     written = 0;
    size_t  amount;
    bool    rejected_bad_specifier = false;

    while( *format != '\0' ) {
        if( *format != '%' ) {
        print_c:
            amount = 1;
            while( format[amount] && format[amount] != '%' )
                amount++;
            print( format, amount );
            format += amount;
            written += amount;
            continue;
        }

        const char* format_begun_at = format;

        if( *( ++format ) == '%' )
            goto print_c;

        if( rejected_bad_specifier ) {
        incomprehensible_conversion:
            rejected_bad_specifier = true;
            format = format_begun_at;
            goto print_c;
        }

        if( *format == 'c' ) {
            format++;
            char c = ( char ) va_arg( parameters, int );
            print( &c, sizeof( c ) );
            written++;
        }
        else if( *format == 's' ) {
            format++;
            const char* str = va_arg( parameters, const char* );
            int len = strlen( str );
            print( str, len );
            written += len;
        }
        else if( *format == 'd' ) {
            format++;
            int val = va_arg( parameters, int );
            char buffer[32];
            itoa( val, buffer, 10 );
            int len = strlen( buffer );
            print( buffer, len );
            written += len;
        }
        else if( *format == 'u' ) {
            format++;
            unsigned int val = va_arg( parameters, unsigned int );
            char buffer[32];
            utoa( val, buffer, 10 );
            int len = strlen( buffer );
            print( buffer, len );
            written += len;
        }
        else if( *format == 'x' ) {
            format++;
            unsigned long val = va_arg( parameters, unsigned long );
            char buffer[32];
            utoa( val, buffer, 16 );
            int len = strlen( buffer );
            print( buffer, len );
            written += len;
        }
        else if( *format == 'o' ) {
            format++;
            unsigned long val = va_arg( parameters, unsigned long );
            char buffer[32];
            utoa( val, buffer, 8 );
            int len = strlen( buffer );
            print( buffer, len );
            written += len;
        }
        else {
            goto incomprehensible_conversion;
        }
    }
    va_end( parameters );
    return written;
}
