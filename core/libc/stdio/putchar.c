// #####################################################################
// Implementation of the putchar function defined in stdio.h
// #####################################################################

#include <stdio.h>

/* Check if the code is part of the kernel. If yes, we can directly use the
tty routines. If not, we would need to use the write-string system call */
#if defined(__is_os1_kernel)
#include <kernel/tty.h>
#endif

// -------------------------------------------------------------
// Print a single character to the screen, and return the ASCII
// value of the character
// PARAM :-
//  int character_to_print
// RET   :-
//  int ascii_value_of_character
// -------------------------------------------------------------

int
putchar( int ci ) {
    char c = ( char ) ci;
#if defined(__is_os1_kernel)
    terminal_write( &c, 1 );
#else
    // TODO: Implement sys_write
#endif
    return ci;
}
