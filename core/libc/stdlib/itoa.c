// #####################################################################
// Implementation of the itoa function defined in stdlib.h
// #####################################################################

#include <stdlib.h>

/* Convert a signed integer into its equivalent string representation. Write
to given buffer and return a pointer to that buffer */

char*
itoa( long num, char* buffer, unsigned long base ) {
    if( num == 0 || base == 0 || base > 16 ) {
        buffer[0] = '0';
        buffer[1] = '\0';
        return buffer;
    }
    /* Assuming that only decimal numbers should be represented as negative
    numbers if required. Others as unsigned, with the MSB set */
    if( base != 10 || num > 0 )
        return utoa( num, buffer, base );
    /* These are negative decimal numbers */
    buffer[0] = '-';
    utoa( num * -1, &buffer[1], base );
    return buffer;
}
