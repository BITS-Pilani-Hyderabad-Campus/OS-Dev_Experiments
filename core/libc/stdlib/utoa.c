// #####################################################################
// Implementation of the utoa function defined in stdlib.h
// #####################################################################

#include <stdlib.h>

/* Global variables */
/* A list of characters used to represent numbers, based on index. */
static const char hex_chars[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
/*A temporary buffer used for conversion */
static char temp[32];

/* Convert an unsigned integer into its equivalent string representation */
char*
utoa( unsigned long num, char* buffer, unsigned long base ) {
    int     temp_index      = 0,
            buffer_index    = 0;
    if( num == 0 || base == 0 || base > 16 ) {
        buffer[0] = '0';
        buffer[1] = '\0';
        return buffer;
    }

    while( num > 0 ) {
        temp[temp_index] = hex_chars[num % base];
        num = num / base;
        temp_index++;
    }
    temp_index--;

    while( temp_index >= 0 ) {
        buffer[buffer_index] = temp[temp_index];
        buffer_index++;
        temp_index--;
    }
    buffer[buffer_index] = '\0';

    return buffer;
}
