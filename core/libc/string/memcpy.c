// #####################################################################
// Implementation of memcpy function as defined in string.h
// #####################################################################

#include <string.h>
//#include <stdint.h>
void*
memcpy( void* restrict dest_ptr, const void* restrict src_ptr, size_t size ) {
	unsigned char* dest = ( unsigned char* ) dest_ptr;
	const unsigned char* src = ( const unsigned char* ) src_ptr;
	for ( size_t i = 0; i < size; i++ )
		dest[i] = src[i];
	return dest_ptr;
}

// #####################################################################
// Internal functions
// #####################################################################

// -------------------------------------------------------------
// Memcpy for byte-by-byte transfer
// -------------------------------------------------------------

/*
static
inline
void*
memcpy_byte(
    void* restrict          dest_ptr,
    const void* restrict    src_ptr,
    size_t                  len
)
{
    uint8_t* dest = ( uint8_t* ) dest_ptr;
    const uint8_t* src = ( const uint8_t* ) src_ptr;
    for( size_t i = 0; i < len; i++ )
        dest[i] = src[i];
    return dest_ptr;
}

// -------------------------------------------------------------
// Memcpy for word-by-word transfer
// -------------------------------------------------------------

static
void*
memcpy_word(
    void* restrict          dest_ptr,
    const void* restrict    src_ptr,
    size_t                  len
)
{
    uint16_t* dest = ( uint16_t* ) dest_ptr;
    const uint16_t* src = ( const uint16_t* ) src_ptr;
    while( len >= 2 ) {
        *dest = *src;
        dest++;
        src++;
        len -= 2;
    }

    uint8_t* b_dest = ( uint8_t* ) dest;
    const uint8_t* b_src = ( const uint8_t* ) b_src;
    while( len > 0 ) {
        *b_dest = *b_src;
        b_dest++;
        b_src++;
        len--;
    }

    return dest_ptr;
}

// -------------------------------------------------------------
// Memcpy for dword-by-dword transfer
// -------------------------------------------------------------

static
void*
memcpy_dword(
    void* restrict          dest_ptr,
    const void* restrict    src_ptr,
    size_t                  len
)
{
    uint32_t* dest = ( uint32_t* ) dest_ptr;
    const uint32_t* src = ( const uint32_t* ) src_ptr;
    while( len >= 4 ) {
        *dest = *src;
        dest++;
        src++;
        len -= 4;
    }

    uint8_t* b_dest = ( uint8_t* ) dest;
    const uint8_t* b_src = ( const uint8_t* ) b_src;
    while( len > 0 ) {
        *b_dest = *b_src;
        b_dest++;
        b_src++;
        len--;
    }

    return dest_ptr;
}

#if defined(__is_x86_64_libc)
static
void*
memcpy_1word(
    void* restrict          dest_ptr,
    const void* restrict    src_ptr,
    size_t                  len
)
{
    uint64_t* dest = ( uint64_t* ) dest_ptr;
    const uint64_t* src = ( const uint64_t* ) src_ptr;
    while( len >= 8 ) {
        *dest = *src;
        dest++;
        src++;
        len -= 8;
    }

    uint8_t* b_dest = ( uint8_t* ) dest;
    const uint8_t* b_src = ( const uint8_t* ) b_src;
    while( len > 0 ) {
        *b_dest = *b_src;
        b_dest++;
        b_src++;
        len--;
    }

    return dest_ptr;
}
#endif

// #####################################################################
// Interface function implementation
// #####################################################################

// -------------------------------------------------------------
// Move the given number of bytes of memory from src to dest.
// Doesn't check for overlapping memory. Returns a pointer to dest
// PARAM :-
//  void* dest
//  const void* src
//  size_t number_of_bytes
// RET   :-
//  void*   dest
// -------------------------------------------------------------

void*
memcpy(
    void* restrict          dest_ptr,
    const void* restrict    src_ptr,
    size_t                  len
)
{
    if( len < 128 || ( dest_ptr & 0x01 ) || ( src_ptr & 0x01 ) ) {
        return memcpy_byte( dest_ptr, src_ptr, len );
    }

#if defined(__is_x86_64_libc)
    else if( !( dest_ptr & 0x07 ) && !( src_ptr & 0x07) ) {
        return memcpy_qword( dest_ptr, src_ptr, len );
    }
#endif

    else if( !( dest_ptr & 0x03 ) && !( src_ptr & 0x03 ) ) {
        return memcpy_dword( dest_ptr, src_ptr, len );
    }
    else {
        return memcpy_word( dest_ptr, src_ptr, len );
    }
}

*/

//TODO: Figure out why stdint.h is not working
