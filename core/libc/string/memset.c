// #####################################################################
// Implementation of function memset as defined in string.h
// #####################################################################

#include <string.h>

// TODO: Need to optimize this

void*
memset( void* buffer_ptr, int val, size_t len ) {
	unsigned char* buffer = ( unsigned char* ) buffer_ptr;
	for ( size_t i = 0; i < len; i++ )
		buffer[i] = ( unsigned char ) val;
	return buffer_ptr;
}
