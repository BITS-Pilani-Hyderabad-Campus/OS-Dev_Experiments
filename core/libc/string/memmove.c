// #####################################################################
// Implementation of memovefunction as defined in string.h
// #####################################################################

#include <string.h>

// TODO: Need to optimize this

void*
memmove( void* dest_ptr, const void* src_ptr, size_t len ) {
    unsigned char* dest = (unsigned char*) dest_ptr;
	const unsigned char* src = (const unsigned char*) src_ptr;
	if ( dest < src )
		for ( size_t i = 0; i < len; i++ )
			dest[i] = src[i];
	else
		for ( size_t i = len; i != 0; i-- )
			dest[i-1] = src[i-1];
	return dest_ptr;
}
