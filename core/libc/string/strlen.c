// #####################################################################
// Implementation of the strlen function defined in string.h
// #####################################################################

#include <string.h>

// -------------------------------------------------------------
// Return the length in bytes of a null-terminated string
// PARAM :-
//  const char* str
// RET   :-
//  size_t length
// -------------------------------------------------------------

size_t
strlen( const char* str ) {
    size_t len = 0;
    while( str[len] )
        len++;
    return len;
}
