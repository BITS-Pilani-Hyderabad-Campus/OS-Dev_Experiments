// #####################################################################
// Implementation of function memcmp defined in string.h
// #####################################################################

#include <string.h>

// -------------------------------------------------------------
// Compare given byte of memory at the two given location
// PARAM :-
//  const void* a_ptr
//  const void* b_ptr
//  size_t number_of_bytes
// RET   :-
//  int ( 0 if equal, -1 if a is smaller, 1 is a is larger )
// -------------------------------------------------------------

int
memcmp( const void* a_ptr, const void* b_ptr, size_t len ) {
    const unsigned char* a = ( const unsigned char* ) a_ptr;
    const unsigned char* b = ( const unsigned char* ) b_ptr;
    for( size_t i = 0; i < len; i++ )
        if( a[i] < b[i] )
            return -1;
        else if( a[i] > b[i] )
            return 1;
    return 0;
}
