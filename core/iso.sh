#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/os1.kernel isodir/boot/os1.kernel
cat > isodir/boot/grub/grub.cfg << EOF
set default=0
set timeout=0
menuentry "OS_1" {
	multiboot2 /boot/os1.kernel
  boot
}
EOF
grub-mkrescue -o os1.iso isodir -d /usr/lib/grub/i386-pc
